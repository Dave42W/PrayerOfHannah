use sea_orm_migration::prelude::*;

use crate::m20231023_125916_song_table;
use crate::m20231022_013312_author_table;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {

        manager
            .create_table(
                Table::create()
                    .table(SongAuthor::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(SongAuthor::SongId).integer().not_null())
                    .col(ColumnDef::new(SongAuthor::AuthorId).integer().not_null())
                    .col(ColumnDef::new(SongAuthor::CreatedTimestamp).timestamp().not_null())
                    .col(ColumnDef::new(SongAuthor::UpdatedTimestamp).timestamp().not_null())
                    .primary_key(
                        Index::create()
                            .name("pk-song_author")
                            .col(SongAuthor::SongId)
                            .col(SongAuthor::AuthorId)
                            .primary(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("FK_SongAuthorSongId")
                            .from(SongAuthor::Table, SongAuthor::SongId)
                            .to(m20231023_125916_song_table::Song::Table, m20231023_125916_song_table::Song::Id)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Restrict)
                    )                    
                    .foreign_key(
                        ForeignKey::create()
                            .name("FK_SongAuthorAuthorId")
                            .from(SongAuthor::Table, SongAuthor::AuthorId)
                            .to(m20231022_013312_author_table::Author::Table, m20231022_013312_author_table::Author::Id)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Restrict)
                    )                    
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-SongAuthor-Song")
                    .table(SongAuthor::Table)
                    .col(SongAuthor::SongId)
                    .to_owned(),
            )
            .await?;

            manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-SongAuthor-Author")
                    .table(SongAuthor::Table)
                    .col(SongAuthor::AuthorId)
                    .to_owned(),
            )
            .await?;
        
        Ok(()) // All good!
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager.drop_index(Index::drop().name("pk-song_author").to_owned())
        .await?;
        
        manager.drop_index(Index::drop().name("idx-SongAuthor-Song").to_owned())
        .await?;

        manager.drop_index(Index::drop().name("idx-SongAuthor-Author").to_owned())
       .await?;

        manager.drop_table(Table::drop().table(SongAuthor::Table).to_owned())
        .await?;
    
        Ok(()) // All good!
     }
}

#[derive(DeriveIden)]
enum SongAuthor {
    Table,
    SongId,
    AuthorId,
    CreatedTimestamp,
    UpdatedTimestamp,
}