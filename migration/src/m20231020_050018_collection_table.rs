use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {

        manager
            .create_table(
                Table::create()
                    .table(Collection::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Collection::Id).integer().not_null().auto_increment().primary_key())
                    .col(ColumnDef::new(Collection::Code).string().not_null())
                    .col(ColumnDef::new(Collection::Name).string().not_null())
                    .col(ColumnDef::new(Collection::Url).string().not_null())
                    .col(ColumnDef::new(Collection::CreatedTimestamp).timestamp().not_null())
                    .col(ColumnDef::new(Collection::UpdatedTimestamp).timestamp().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-collection-name")
                    .table(Collection::Table)
                    .col(Collection::Name)
                    .unique()                  
                    .to_owned(),
            )
            .await?;

            manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-collection-code")
                    .table(Collection::Table)
                    .col(Collection::Code)
                    .unique()                  
                    .to_owned(),
            )
            .await?;

        Ok(()) // All good!
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        
        manager.drop_index(Index::drop().name("idx-collection-name").to_owned())
        .await?;
        
        manager.drop_index(Index::drop().name("idx-collection-code").to_owned())
        .await?;
        
        manager.drop_table(Table::drop().table(Collection::Table).to_owned())
        .await?;
    
        Ok(()) // All good!
    }
}

#[derive(DeriveIden)]
pub enum Collection {
    Table,
    Id,
    Code,
    Name,
    Url,
    CreatedTimestamp,
    UpdatedTimestamp,
}
