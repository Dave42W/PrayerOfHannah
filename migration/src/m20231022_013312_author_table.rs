use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {

        manager
            .create_table(
                Table::create()
                    .table(Author::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Author::Id).integer().not_null().auto_increment().primary_key())
                    .col(ColumnDef::new(Author::FirstName).string().not_null())
                    .col(ColumnDef::new(Author::Surname).string().not_null())
                    .col(ColumnDef::new(Author::CreatedTimestamp).timestamp().not_null())
                    .col(ColumnDef::new(Author::UpdatedTimestamp).timestamp().not_null())
                    .to_owned(),
            )
            .await?;

        manager
        .create_index(
            Index::create()
                .if_not_exists()
                .name("idx-author-name")
                .table(Author::Table)
                .col(Author::Surname)
                .col(Author::FirstName)
                .unique()                  
                .to_owned(),
        )
        .await?;

        Ok(()) // All good!

    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager.drop_index(Index::drop().name("idx-author-name").to_owned())
        .await?;
        
        manager.drop_table(Table::drop().table(Author::Table).to_owned())
        .await?;
    
        Ok(()) // All good!
    }
}

#[derive(DeriveIden)]
pub enum Author {
    Table,
    Id,
    FirstName,
    Surname,
    CreatedTimestamp,
    UpdatedTimestamp,
}
