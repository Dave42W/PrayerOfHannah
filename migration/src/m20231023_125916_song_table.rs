use sea_orm_migration::prelude::*;

use crate::m20231020_050018_collection_table;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {

        manager
            .create_table(
                Table::create()
                    .table(Song::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Song::Id).integer().not_null().auto_increment().primary_key())
                    .col(ColumnDef::new(Song::CollectionId).integer().not_null())
                    .col(ColumnDef::new(Song::Number).integer().not_null())
                    .col(ColumnDef::new(Song::Title).string().not_null())
                    .col(ColumnDef::new(Song::CreatedTimestamp).timestamp().not_null())
                    .col(ColumnDef::new(Song::UpdatedTimestamp).timestamp().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("FK_SongCollectionId")
                            .from(Song::Table, Song::CollectionId)
                            .to(m20231020_050018_collection_table::Collection::Table, m20231020_050018_collection_table::Collection::Id)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Restrict)
                    )                    
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-Song-Title")
                    .table(Song::Table)
                    .col(Song::Title)
                    .col(Song::CollectionId)
                    .unique()                  
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx-Song-Collection-Number")
                    .table(Song::Table)
                    .col(Song::CollectionId)
                    .col(Song::Number)
                    .unique()                  
                    .to_owned(),
            )
            .await?;

        
        Ok(()) // All good!
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        
        manager.drop_index(Index::drop().name("idx-Song-Title").to_owned())
        .await?;
        
        manager.drop_index(Index::drop().name("idx-Song-Collection-Number").to_owned())
        .await?;

        manager.drop_table(Table::drop().table(Song::Table).to_owned())
        .await?;
    
        Ok(()) // All good!
    }
}

#[derive(DeriveIden)]
pub enum Song {
    Table,
    Id,
    CollectionId,
    Number,
    Title,
    CreatedTimestamp,
    UpdatedTimestamp,
}
