# Prayer of Hannah: Technologies

## License

All contributions to be signed off as AGPL 3

## Source Code

- CodeBerg

## Language and tools

- Rust
- Axum
- HandlebarsRs

All code should meet Rust formatting standards, strict Clipper linting. Until version 1.0 is released the latest release of all tools should be used.

## Data

- SurrealDb
- OpenLyrics (song import/export)

### dbms

 Migration scripts should create and update all dbms structures (and the database if it does not exist)

### coding

Locale should be from the system with fallback.
eg en-GB and en-US should provide for British and American languages. If either is unavailable then fall back to en, if that is not available fallback to the default.

## Clients

### Remote Controller

HTML 5 Form

### Data Entry

- HTML 5, CSS
- do not accept html in data entry (only MarkDown)
- do not store html (only MarkDown)

### Service Viewer

Browser, HTML 5, full screen (kiosk mode if available)

### Fonts

Use "Atkinson+Hyperlegible" as the default font

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible">

## Documentation

Make sure that every step from installing rust to working development system is clearly documented and up-to-date so that it is easy to get started with development.
