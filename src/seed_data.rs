// Prayer of Hannah
// Free Software to provide Slides as a web service for Worship, Noticeboards and more. 
// Named in honour of Hannah (see 1 Samuel 2:1-10) and particularly from verse 8: 
//"He raises up the poor from the dust; he lifts the needy from the ash heap" 
// Copyright (C) 2023  Dave Warnock dwarnock@gmail.com

// This program is free software: you can redistribute it and/or modify it under the terms
// of the GNU Affero General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://www.gnu.org/licenses/>.

// Source code at https://codeberg.org/Dave42W/PrayerOfHannah

use entity::collection::BasicCollection as BasicCollectionModel;
use entity::collection::Model as CollectionModel;
use entity::collection;
use entity::prelude::Collection;

use entity::author::BasicAuthor as BasicAuthorModel;
use entity::author::Model as AuthorModel;
use entity::author;
use entity::prelude::Author;

use entity::song::BasicSong as BasicSongModel;
use entity::song::Model as SongModel;
use entity::song;
use entity::prelude::Song;

use entity::song_author::BasicSongAuthor as BasicSongAuthorModel;
use entity::song_author::Model as SongAuthorModel;
use entity::song_author;
use entity::prelude::SongAuthor;


use sea_orm::DatabaseTransaction;

use sea_orm::{DatabaseConnection, TransactionTrait, DbErr, IntoActiveModel, EntityTrait, QueryFilter, ColumnTrait, ActiveModelTrait};

pub async fn seed(db: &DatabaseConnection) -> Result<(), DbErr> {
    println!("seeding:");
    let txn = db.begin().await?;
    seed_collection(&txn).await?;
    seed_author(&txn).await?;
    seed_song(&txn).await?;
    seed_song_author(&txn).await?;
    txn.commit().await?;    
    println!("");
    println!("done");
    Ok(())
 }

 pub async fn seed_collection(txn: &DatabaseTransaction) -> Result<(), DbErr> {
    insert_collection(&txn, 
        "StF", 
        "Singing the Faith", 
        "https://www.methodist.org.uk/our-faith/worship/singing-the-faith-plus/"
    )
    .await.unwrap();

    insert_collection(&txn,
        "H&P",
        "Hymns & Psalms",
        ""
    )
    .await.unwrap();

    insert_collection(&txn,
        "SoF1",
        "Songs of Fellowship book 1",
        ""
    )
    .await.unwrap();

    Ok(())
}

pub async fn seed_author(txn: &DatabaseTransaction) -> Result<(), DbErr> {
    insert_author(&txn, 
        "Charles", 
        "Wesley", 
    )
    .await.unwrap();

    insert_author(&txn,
        "John",
        "Wesley",
    )
    .await.unwrap();
    
    insert_author(&txn,
        "John",
        "Bell",
    )
    .await.unwrap();
    
    insert_author(&txn,
        "Graham",
        "Maule",
    )
    .await.unwrap();
    
    Ok(())
}

pub async fn seed_song(txn: &DatabaseTransaction) -> Result<(), DbErr> {
    insert_song(&txn, 
        "StF", 
        202,
        "Hark! The herald-angels sing"
    )
    .await.unwrap();

    insert_song(&txn, 
        "H&P", 
        106,
        "Hark! The herald-angels sing"
    )
    .await.unwrap();

    insert_song(&txn, 
        "StF", 
        5,
        "Father, in whom we live"
    )
    .await.unwrap();

    insert_song(&txn, 
        "StF", 
        671,
        "What shall we offer our good Lord"
    )
    .await.unwrap();

    insert_song(&txn, 
        "StF", 
        49,
        "God beyond all names"
    )
    .await.unwrap();
 
    insert_song(&txn, 
        "StF", 
        101,
        "Before the world began"
    )
    .await.unwrap();
 
    insert_song(&txn, 
        "StF", 
        1,
        "All people that on earth do dwell"
    )
    .await.unwrap();
 
    Ok(())
}

pub async fn seed_song_author(txn: &DatabaseTransaction) -> Result<(), DbErr> {
    insert_song_author(&txn, 
        "StF", 
        202,
        "Charles", 
        "Wesley", 
    )
    .await.unwrap();

    insert_song_author(&txn, 
        "H&P", 
        106,
        "Charles", 
        "Wesley", 
    )
    .await.unwrap();

    insert_song_author(&txn, 
        "StF", 
        5,
        "Charles", 
        "Wesley", 
    )
    .await.unwrap();

    insert_song_author(&txn, 
        "StF", 
        671,
        "John", 
        "Wesley", 
    )
    .await.unwrap();

    insert_song_author(&txn, 
        "StF", 
        49,
        "John", 
        "Bell", 
    )
    .await.unwrap();
 
    insert_song_author(&txn, 
        "StF", 
        101,
        "John", 
        "Bell", 
    )
    .await.unwrap();
 
    insert_song_author(&txn, 
        "StF", 
        101,
        "Graham", 
        "Maule", 
    )
    .await.unwrap();
 
    Ok(())
}

async fn insert_collection(txn: &DatabaseTransaction, code: &str, name: &str, url: &str)  -> Result<(), DbErr> {
    let c = BasicCollectionModel {
        id: 0,
        code: code.to_owned(),
        name: name.to_owned(),
        url: url.to_owned(),
    };

    let cf: Vec<CollectionModel> = Collection::find()
    .filter(collection::Column::Name.eq(&c.name))
    .all(txn)
    .await?;   

    if cf.is_empty() {
        let am = c.into_active_model();

        am.insert(txn).await.expect("could not insert");
        print!(".");
    }
    Ok(())
}

async fn insert_author(txn: &DatabaseTransaction, first_name: &str, surname: &str)  -> Result<(), DbErr> {
    let c = BasicAuthorModel {
        id: 0,
        first_name: first_name.to_owned(),
        surname: surname.to_owned(),
    };

    let cf: Vec<AuthorModel> = Author::find()
    .filter(author::Column::FirstName.eq(&c.first_name))
    .filter(author::Column::Surname.eq(&c.surname))
    .all(txn)
    .await?;   

    if cf.is_empty() {
        let am = c.into_active_model();

        am.insert(txn).await.expect("could not insert");
        print!(".");
    }
    Ok(())
}

async fn insert_song(txn: &DatabaseTransaction, code: &str, number: i32, title: &str)  -> Result<(), DbErr> {
    let cf: Vec<CollectionModel> = Collection::find()
    .filter(collection::Column::Code.eq(code))
    .all(txn)
    .await?;   

    if !cf.is_empty() {
        let cn: i32 = cf[0].id;
        let s = BasicSongModel {
            id: 0,
            collection_id: cn,
            number: number,
            title: title.to_owned(),
        };

        let st: Vec<SongModel> = Song::find()
        .filter(song::Column::Title.eq(&s.title))
        .filter(song::Column::CollectionId.eq(&s.collection_id.to_string()))
        .all(txn)
        .await?;   

        let sn: Vec<SongModel> = Song::find()
        .filter(song::Column::Number.eq(&s.number.to_string()))
        .filter(song::Column::CollectionId.eq(&s.collection_id.to_string()))
        .all(txn)
        .await?;   

        if st.is_empty() && sn.is_empty() {
            let am = s.into_active_model();

            am.insert(txn).await.expect("could not insert");
            print!(".");
        }
    }
    Ok(())
}

async fn insert_song_author(txn: &DatabaseTransaction, code: &str, number: i32, first_name: &str, surname: &str)  -> Result<(), DbErr> {
    let cf: Vec<CollectionModel> = Collection::find()
    .filter(collection::Column::Code.eq(code))
    .all(txn)
    .await?;   

    if !cf.is_empty() {
        let cn: i32 = cf[0].id;

        let sn: Vec<SongModel> = Song::find()
        .filter(song::Column::Number.eq(number.to_string()))
        .filter(song::Column::CollectionId.eq(cn.to_string()))
        .all(txn)
        .await?;   

        let a: Vec<AuthorModel> = Author::find()
        .filter(author::Column::FirstName.eq(first_name.to_string()))
        .filter(author::Column::Surname.eq(surname.to_string()))
        .all(txn)
        .await?;   

        if !sn.is_empty() && !a.is_empty() {
            let sid: i32 = sn[0].id;
            let aid: i32 = a[0].id;

            let cf: Vec<SongAuthorModel> = SongAuthor::find()
            .filter(song_author::Column::SongId.eq(sid.to_string()))
            .filter(song_author::Column::AuthorId.eq(aid.to_string()))
            .all(txn)
            .await?;   
        
            if cf.is_empty() {
        
                let sa = BasicSongAuthorModel {
                    song_id: sid,
                    author_id: aid,
                };
                
                let am = sa.into_active_model();

                am.insert(txn).await.expect("could not insert");
                print!(".");
            }
        }
    }
    Ok(())
}

