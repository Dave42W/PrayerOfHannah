use std::{str::FromStr, fmt};

use serde::{Deserializer, Deserialize, de};
use url::form_urlencoded::{byte_serialize, parse};

pub fn url_encode(from: String) -> String {
    return byte_serialize(from.as_bytes()).collect();
}

pub fn url_decode(from: String) -> String {
    return parse(from.as_bytes())
        .map(|(key, val)| [key, val].concat())
        .collect();
}

pub fn empty_string_as_none<'de, D, T>(de: D) -> Result<Option<T>, D::Error>
where
    D: Deserializer<'de>,
    T: FromStr,
    T::Err: fmt::Display,
{
    println!("help pre-panic");
    let opt = Option::<String>::deserialize(de)?;
    match opt.as_deref() {
        None | Some("") => Ok(None),
        Some(s) => FromStr::from_str(s).map_err(de::Error::custom).map(Some),
    }
}
