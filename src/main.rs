// Prayer of Hannah
// Free Software to provide Slides as a web service for Worship, Noticeboards and more. 
// Named in honour of Hannah (see 1 Samuel 2:1-10) and particularly from verse 8: 
//"He raises up the poor from the dust; he lifts the needy from the ash heap" 
// Copyright (C) 2023  Dave Warnock dwarnock@gmail.com

// This program is free software: you can redistribute it and/or modify it under the terms
// of the GNU Affero General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://www.gnu.org/licenses/>.

// Source code at https://codeberg.org/Dave42W/PrayerOfHannah

use handlebars::Handlebars;
use dotenvy::dotenv;
use sea_orm::{DatabaseConnection, Database};
use migration::{Migrator, MigratorTrait};

use std::env;

mod seed_data;



mod routes;
mod controllers;
mod templates;

use crate::{templates::get_initialized_handlebars, seed_data::seed};

#[derive(Clone)]
pub struct AppState<'a> {
    pub handlebars: Handlebars<'a>,
    pub db: DatabaseConnection,
    pub uri: String,
}

#[tokio::main]
async fn main() {
    dotenv().expect(".env file not found");

    let templates_dir = env::var("TEMPLATES_DIR").unwrap(); 
    let server_uri = env::var("SERVER_URI").unwrap();
    let db_uri: String = env::var("DATABASE_URL").unwrap();
    let d: DatabaseConnection = Database::connect(db_uri).await.unwrap();
    

    println!("Before Migration");
    Migrator::up(&d, None).await.unwrap();
    println!("After Migration");

    println!("Before Seeding");    
    seed(&d).await.unwrap();
    //seed_collection(&d).await.unwrap();
    println!("After Seeding");

    let state = AppState {
        handlebars: get_initialized_handlebars(templates_dir),
        db: d,
        uri: server_uri,
    };

    run(state).await
}

pub async fn run(state: AppState<'static>) {
    let server_uri = state.uri.clone();
    let app = routes::create_routes(state);
    
    axum::Server::bind(&server_uri.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
