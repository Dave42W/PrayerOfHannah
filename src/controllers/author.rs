// Prayer of Hannah
// Free Software to provide Slides as a web service for Worship, Noticeboards and more. 
// Named in honour of Hannah (see 1 Samuel 2:1-10) and particularly from verse 8: 
//"He raises up the poor from the dust; he lifts the needy from the ash heap" 
// Copyright (C) 2023  Dave Warnock dwarnock@gmail.com

// This program is free software: you can redistribute it and/or modify it under the terms
// of the GNU Affero General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://www.gnu.org/licenses/>.

// Source code at https://codeberg.org/Dave42W/PrayerOfHannah

use std::collections::BTreeMap;

use axum::{extract::{State, Path}, response::{IntoResponse, Html, Redirect}, Form};
use ::entity::author::BasicAuthor as BasicAuthorModel;
use ::entity::author;
use ::entity::prelude::Author;
use sea_orm::{ActiveModelTrait, EntityTrait, Set, QueryOrder, IntoActiveModel};

use crate::AppState;

pub async fn author_list(State(state): State<AppState<'_>>) -> impl IntoResponse {
    let s = state;

    let a: Vec<author::Model> = Author::find().order_by_asc(author::Column::Surname).order_by_asc(author::Column::FirstName).all(&s.db).await
    .expect("could not find any authors");

    let mut data = BTreeMap::new();
    data.insert("author".to_string(), a);
    Html(s.handlebars.render("author.html", &data).unwrap()).into_response()
}

pub async fn author_display(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    let i = id.parse::<i32>().unwrap();
	
    let author = Author::find_by_id(i).one(&s.db).await
    .expect("could not find the author");
    let m = author.unwrap();
    let mut data = BTreeMap::new();
    data.insert("id".to_string(), m.id.to_string());
    data.insert("surname".to_string(), m.surname);
    data.insert("first_name".to_string(), m.first_name);
    Html(s.handlebars.render("author_display.html", &data).unwrap()).into_response()
}


pub async fn new_author_form(State(state): State<AppState<'_>>) -> impl IntoResponse {
    let s = state;
    let mut data = BTreeMap::new();
    data.insert("method".to_string(), "post");
    data.insert("id".to_string(), "0");
    Html(s.handlebars.render("author_form.html", &data).unwrap()).into_response()
}

pub async fn insert_author(State(state): State<AppState<'_>>, Form(input): Form<BasicAuthorModel>) -> impl IntoResponse {
    let s = state;

    let am = input.into_active_model();
    am.insert(&s.db).await.expect("could not insert");

    Redirect::to("/Author")
}

pub async fn edit_author_form(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    let i = id.parse::<i32>().unwrap();
	
    let author = Author::find_by_id(i).one(&s.db).await
    .expect("could not find the author");
    let m = author.unwrap();
    let mut data = BTreeMap::new();
    data.insert("id".to_string(), m.id.to_string());
    data.insert("method".to_string(), "patch".to_string());
    data.insert("surname".to_string(), m.surname);
    data.insert("first_name".to_string(), m.first_name);
    Html(s.handlebars.render("author_form.html", &data).unwrap()).into_response()
}

pub async fn update_author(State(state): State<AppState<'_>>, Form(input): Form<BasicAuthorModel>) -> impl IntoResponse {
    let s = state;
    
    let i = input.id.to_owned();
    let author = Author::find_by_id(i).one(&s.db).await
    .expect("could not find the author");
    let mut am: author::ActiveModel = author.unwrap().into();    
	
    am.surname = Set(input.surname.to_owned());
    am.first_name = Set(input.first_name.to_owned());
    
    am.update(&s.db).await.expect("could not update");
    Redirect::to("/Author")    
}

pub async fn delete_author(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    
    let i = id.parse::<i32>().unwrap();
    let author = Author::find_by_id(i).one(&s.db).await
    .expect("could not find the author");
    let am: author::ActiveModel = author.unwrap().into();    
	 
    am.delete(&s.db).await.expect("could not delete");
    Redirect::to("/Author")        
}