// Prayer of Hannah
// Free Software to provide Slides as a web service for Worship, Noticeboards and more. 
// Named in honour of Hannah (see 1 Samuel 2:1-10) and particularly from verse 8: 
//"He raises up the poor from the dust; he lifts the needy from the ash heap" 
// Copyright (C) 2023  Dave Warnock dwarnock@gmail.com

// This program is free software: you can redistribute it and/or modify it under the terms
// of the GNU Affero General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://www.gnu.org/licenses/>.

// Source code at https://codeberg.org/Dave42W/PrayerOfHannah

use std::collections::BTreeMap;

use axum::{extract::{State, Path}, response::{IntoResponse, Html, Redirect}, Form};
use entity::{collection, song::ExtendedSong};
use entity::prelude::Collection;

use entity::author;

use entity::prelude::Author;
use entity::prelude::SongAuthor;

use ::entity::song::BasicSong as BasicSongModel;
use ::entity::song;
use ::entity::prelude::Song;

use sea_orm::{ActiveModelTrait, EntityTrait, Set, QueryOrder, IntoActiveModel, LoaderTrait};
use serde::{Serialize, Deserialize};

use crate::AppState;

pub async fn song_list(State(state): State<AppState<'_>>) -> impl IntoResponse {
    let s = state;

    // get the songs without using the Collection foreign key
    let songs: Vec<song::Model> = Song::find().order_by_asc(song::Column::Title).all(&s.db).await
    .expect("could not find any Songs");

    // get the many to many relationship of authors
    let authors: Vec<Vec<author::Model>> = songs.load_many_to_many(Author, SongAuthor, &s.db).await
    .expect("could not find related authors");

    // get the many to one foreign key to collection
    let collections = songs.load_one(Collection, &s.db).await
    .expect("could not find fk collection");


    let authors = authors.into_iter();
    let collections = collections.into_iter();
    let songs = songs.into_iter().zip(collections).zip(authors);

    let extended_songs: Vec<ExtendedSong> = songs.map(|((song, collection), authors)| {
        let author_names = authors.into_iter().map(|author| format!("<a href=\"/Author/{}\">{} {}</a>; ", author.id, author.first_name, author.surname)).collect::<Vec<String>>().join(" ");
        let collection: (String, String) = collection.map(|collection| (collection.code, collection.name)).unwrap_or((String::new(), String::new()));
        ExtendedSong{
            id: song.id,
            collection_id: 0,
            collection_code: collection.0,
            collection_name: collection.1,
            number: song.number,
            title: song.title,
            authors: author_names,
        }
    }).collect();

    let mut data = BTreeMap::new();
    data.insert("song".to_string(), extended_songs);
    Html(s.handlebars.render("song.html", &data).unwrap()).into_response()
}

#[derive(Serialize, Deserialize)]
struct SongForm {
    collection: Vec<collection::Model>,
    method: String,
    id: String,
    title: String,
    number: String,
    collection_id: i32,
    collection_name: String,
}

pub async fn song_display(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    let i = id.parse::<i32>().unwrap();
	
    let song = Song::find_by_id(i).one(&s.db).await
    .expect("could not find the Song");
    let m = song.unwrap();

    let cm: Vec<collection::Model> = Collection::find().order_by_asc(collection::Column::Name).all(&s.db).await
    .expect("could not find any Collections");

    let cmc = Collection::find_by_id(m.collection_id).one(&s.db).await
    .expect("could not find any Collections");
    let mc = cmc.unwrap();

    let sf = SongForm {
        collection: cm,
        method: "display".to_string(),
        id: m.id.to_string(),
        title: m.title,
        number: m.number.to_string(),
        collection_id: m.collection_id,
        collection_name: mc.name,
        };

    Html(s.handlebars.render("song_display.html", &sf).unwrap()).into_response()
}

pub async fn new_song_form(State(state): State<AppState<'_>>) -> impl IntoResponse {
    let s = state;

    let m: Vec<collection::Model> = Collection::find().order_by_asc(collection::Column::Name).all(&s.db).await
    .expect("could not find any Collections");

    let sf = SongForm {
        collection: m,
        method: "post".to_string(),
        id: "0".to_string(),
        title: "".to_string(),
        number: "".to_string(),
        collection_id: 0,
        collection_name: "".to_string(),
        };
    Html(s.handlebars.render("song_form.html", &sf).unwrap()).into_response()
}

pub async fn insert_song(State(state): State<AppState<'_>>, Form(input): Form<BasicSongModel>) -> impl IntoResponse {
    let s = state;

    let am = input.into_active_model();

    am.insert(&s.db).await.expect("could not insert");

    Redirect::to("/Song")
}

pub async fn edit_song_form(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    let i = id.parse::<i32>().unwrap();
	
    let song = Song::find_by_id(i).one(&s.db).await
    .expect("could not find the Song");
    let m = song.unwrap();

    let cm: Vec<collection::Model> = Collection::find().order_by_asc(collection::Column::Name).all(&s.db).await
    .expect("could not find any Collections");

    let sf = SongForm {
        collection: cm,
        method: "patch".to_string(),
        id: m.id.to_string(),
        title: m.title,
        number: m.number.to_string(),
        collection_id: m.collection_id,
        collection_name: "".to_string(),
        };

    Html(s.handlebars.render("song_form.html", &sf).unwrap()).into_response()
}

pub async fn update_song(State(state): State<AppState<'_>>, Form(input): Form<BasicSongModel>) -> impl IntoResponse {
    let s = state;
    
    let i = input.id.to_owned();
    let song = Song::find_by_id(i).one(&s.db).await
    .expect("could not find the Song");
    let mut am: song::ActiveModel = song.unwrap().into();    
	
    am.collection_id = Set(input.collection_id.to_owned());
    am.number = Set(input.number.to_owned());
    am.title = Set(input.title.to_owned());
    
    am.update(&s.db).await.expect("could not update");
    Redirect::to("/Song")    
}

pub async fn delete_song(State(state): State<AppState<'_>>, Path(id): Path<String>) -> impl IntoResponse {
    let s = state;
    
    let i = id.parse::<i32>().unwrap();
    let song = Song::find_by_id(i).one(&s.db).await
    .expect("could not find the Song");
    let am: song::ActiveModel = song.unwrap().into();    
	 
    am.delete(&s.db).await.expect("could not delete");
    Redirect::to("/Song")        
}