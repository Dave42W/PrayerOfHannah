# System Design: Documentation and plans

## DBMS

We are going to use Sqlite with a separate database table for each organisation. That will allow simple transfer of an organisations entire data between systems.

However, to save a lot of data entry we will provide a way to share resources such as songs between organisations. There is no point in every church typing in (or even importing) their own hymn book that is the same as everyone else in their denomination. Currently I think that will be by marking resources as sharable and that will allow the sytem admin to bring them into a "system" organisation. Every other organisation will be able to import from "system".

## Songs

### Importing

We will import songs from the [OpenLyrics](https://docs.openlyrics.org/en/latest/) format that [OpenLP](https://openlp.org/) developed. That is an XML format with a Relax-NG schema.

### Capabilities

Most worship software has inconvenient limitations, failing to support common use cases. Our design needs to be able to support (eventually):

- switching between live musicians and multiple pre-recorded audio versions. A user may have multiple versions of a hymn with different audio. Toggling audio on/off at the device or sound desk is not a good solution as it is too easy to forget. Sometimes services have mixture so that some songs are with live musicians and others are not.
- many hymns can be sung with different tunes, this needs to be simple to select. The choice needs to be dynamic (even being able to switch tune during a service).
- switching between different numbers of verses. Different audio sources can include different numbers of verses. Also we often miss out verses, for example at funerals. I don't want to change the master copy of a hymn just for one service as it often catches people out when the hymn is next used.
- switching texts. We tend to "customise" some songs eg "He's got the whole world in his hand" where we put the name of the baby in verse 3. So we need a different text just for one service with no risk of overwriting the standard/default version.
- using Video. I want to be able to have a video version of a song (eg from YouTube or one another Church has recorded for us). I may want to just use the video audio and display our words or I might want to put our words on top of the video.
- multiple hymnbooks. Lots of Churches have more than one hymnbook and a song might be in multiple book with some textual differences. There needs to be a quick way to specifiy which version of a song is needed.
- translations. More and more Churches use multiple languages. So it would be good to have more than one language for the same song and display them at the same time (split screen or multiple screens each showing one language).

### Structure

The base building bock is a single song element (verse/chorus/bridge/title). This will "normally" relate to a single slide (there may be edge cases where a verse is so long that it needs to be split over multiple slides or where a verse and chorus are so short they fit onto a single slide). For security/safety the text of slide elements will be stored in [Markdown](https://www.markdownguide.org/) format which prevents any dangerous html, javascript etc being put into a song lyrics.

A Song will be a collection of song elements from a single collection (hymn book whether printed or online). The song can have several instances (with the elements in different orders, different audio). Each instance has a name, when a song is added to a service the user can pick which instance. When a service is running the user can see alternative instances of each song.

Each Song will be in a single language so all the song elements will be in that language.

The same Song can be in the system multiple times. For example from different hymn books or in different languages. The combination of song title, hymn book and language will be unique.

#### For example: Oh Come all ye Faithful

    Song 1:
        Title: Oh Come all ye faithful
        Book: Singing the Faith #212
        Language: English (UK)
        Author:
        Copyright: 

        Element T1:
            O come, all ye faithful
            StF 212
        Element V1:
            O come, all ye faithful,
            joyful and triumphant,
            O come ye, O come ye to Bethlehem;
            come and behold him,
            born the King of angels:
        Element C1:
            O come, let us adore him,
            O come, let us adore him,
            O come, let us adore him, Christ the Lord!
        ...            

        Instance 1:
            Advent (5 verses each with Chorus). 
            No Audio
            Element Display order: T1 V1 C1 V2 C1 V3 C1 V4 C1 V5 C1
        Instance 2:
            Advent (5 verses each with Chorus).
            Audio from StF CD's 
            Element Display order: T1 V1 C1 V2 C1 V3 C1 V4 C1 V5 C1
        Instance 3:
            Advent (5 verses each with Chorus).
            Audio from No Organist no problem
            Element Display order: T1 V1 C1 V2 C1 V3 C1 V4 C1 V5 C1
        Instance 4:
            Video recording from Church x (no overlay)
            Element Display order:
        Instance 5:
            Video recording from Church x (with text overlay)
            Element Display order: T1 V1 C1 V2 C1 V3 C1 V4 C1 V5 C1
        Instance 6:
            Christmas Day (6 verses each with Chorus)
            no audio
        Instance 7:
            Christmas Day (6 verses each with Chorus)
            Audio from StF CD's
    Song 2:
        Title: Oh Come all ye faithful
        Book: Hymns & Psalms #110
        Language: English (UK)
        //different case of words
        ...
    Song 3:
        Title: Oh Come all ye faithful
        Book: Ancient and Modern: Hymns and Sons
        Language: English (UK)
        //God of God" vs "True God of True God"
        ...
    Song 4:
        Title: Ô peuple fidèle
        Book: Any French hymn book
        Language: French
        ...        

## Services

When a service is created it has a default language. Slide elements are added as a simple list (songs, images, bible passages, liturgy, presentation slides, ...) and there is a pointer to the current slide.

Every slide element in a service can have links to that element in any other language than the default. This means that not every slide element in a service needs to be available in every language. It is possible that we may be able to treat music notation (for musicians) as a language.

### Remote Controllers

Remote controllers can change the current slide with some simple commands:

- first
- previous
- next
- last
- next slide element (eg jump past the remaining verses to the next song)
- previous slide element (eg jump to the beginning of the previous song)
- beginning of element (eg go back to the first slide of this song)

The initial expectation is that the remote controller will be a mobile phone with a web browser interface that displays only the remote control buttons. The person operating the remote control will watch the service on a separate device (could be the main screen if they are sitting at the sound desk).

### Service Viewers

A service viewer is any device used to display service slides. It could be:

- a smarttv (providing the web browser is capable).
- a single board computer (eg RaspberryPi connected to a TV or projector)
- a tablet on the lecturn for the preacher
- a tablet held by a member of the congregation making the text more accessible
- a tablet, phone, laptop or tv at a remote site (Creche, Vestibule, Home)

The default mode for a service viewer will be fullscreen with local controls as locked down as possible.

Each organisation has a current service. So a service viewer (browser on any device) automatically loads the current slide of the current service for it's organisation. It registers itself with the server and the server will notify it when the current slide changes, so the viewer can load the new slide (we will use etags to cache slides in the browser) .

Therefore a service viewer has a few configuration items so that the server knows what slide to provide:

- preferred language. If this is different to the service default language then for every service element the service viewer will look for a version of the current slide element in it's own language (for the slide viewers to stay in sync every slide element will need the same number of slides in every language)
- whether sound should be sent to the remote viewer (typically only one viewer per room will have sound). Not only does this prevent interference, it also reduces bandwidth.

For maximum performance, at a site, the server could create static slides for every slide element for every slide viewer, this could include images and video at the appropriate resolution (bandwidth and screen dependant). If the site has a simple caching internet firewall then all the slides for a service could be put into the local cache by running through the service beforehand with the viewers connected.