# Building Prayer of Hannah

## Prerequisites

See the "technology.md" for details of what we use for Prayer of Hannah.

This document assumes you have a working (and up-to-date) RUST installation (until we reach version 1.0 we will track the latest full-release) plus GIT (I, Dave, use with GIT plugin for Visual Studio Code).

## Code

The code is hosted on [CodeBerg](https://codeberg.org) at [PrayerOfHannah](https://codeberg.org/Dave42W/PrayerOfHannah) development should be done in branches and pulled into Master when stable.

Initially, you will need to fork the code base and then clone that new repository.

## Getting started

- The PrayerOfHannah directory should be your working space (where README.md is). From here you will use cargo to build and run the code.
- First, bring in all the RUST packages (listed in Cargo.toml) using "cargo update"
- To compile and run - I use "cargo watch -x run" as it keeps re-building the code as I save changes
- When PrayerOfHannah starts executing
  - if the database file does not exist then a new RocksDB database will be created in the project directory
  - the migration scripts will be automatically run every time to create/update the database schema
